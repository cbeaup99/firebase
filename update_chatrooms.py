import firebase_admin
from firebase_admin import credentials
from firebase_admin import db
import time
import datetime


if __name__=="__main__":
    cred = credentials.Certificate("chatmessenger-6ea89-firebase-adminsdk-cq5gr-812b1f5441.json")
    firebase_admin.initialize_app(cred, {
        'databaseURL': 'https://chatmessenger-6ea89.firebaseio.com'
    })

    ref = db.reference("chatrooms")
    chatrooms = ref.get()
    chatroom_ids = chatrooms.keys()
    for cid in chatroom_ids:
        ref_string = "chatrooms/{}/last_updated".format(cid)
        room_ref = db.reference(ref_string)
        room_ref.set("1")
