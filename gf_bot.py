import firebase_admin
from firebase_admin import credentials
from firebase_admin import db
from firebase_admin import messaging
import time
import datetime
import random
import beta_bot

class BetaBotDM(beta_bot.BetaBotConversation):
    def __init__(self):
        super(BetaBotDM, self).__init__(conversation_id=None)
        self.json = {} 

    def get_subscribed_conversation_ids(self):
        path = self.uid + "/conversations"
        ref = db.reference(path)
        self.json = ref.get()
        convo_ids = self.json.keys()
        return convo_ids

    def send_reply(self, message_text):
        for conversation_id in self.get_subscribed_conversation_ids():
            self.send_bot_message(message_text, conversation_id)
        self.write_to_participant_nodes(message_text)
        self.notify_participants(message_text)

    def send_bot_message(self, message_text, conversation_id):
        message_id = "BetaBot" + str(int(time.time()))
        ref_string = self.get_reference_string(message_id, conversation_id)
        ref = db.reference(ref_string)
        message = self.get_message_dict(message_text, message_id)
        ref.set(message)

    def get_reference_string(self, message_id, conversation_id):
        location = "conversations/{}/messages/{}"
        location = location.format(conversation_id, message_id)
        return location

    def write_to_participant_nodes(self, message):
        id_dict = self.get_ids()
        date = datetime.datetime.now().strftime("%Y-%m-%dT%H:%M:%SZ")
        for uid, convo_id in id_dict.iteritems():
            path = uid + "/conversations/" + convo_id
            path += "/latest_message"
            ref = db.reference(path)
            ref.set({
                    "date": date,
                    "isRead": False,
                    "message": message,
                    "opened": False
                })

    def get_ids(self):
        users_to_message = {}# {uid : convo_id}
        for key, value in self.json.iteritems():
            recipient_uid = [uid for uid in value["uid"].split(", ") if uid != "gf-bot"][0]
            convo_id = key
            users_to_message[recipient_uid] = convo_id
        return users_to_message

    def get_participants(self):
        conversations = self.json.values()
        all_recipients = [convo["uid"].split(", ") for convo in conversations]
        participants = [uid for convo in all_recipients for uid in convo]
        participants = [uid for uid in participants if uid != self.uid]
        return participants 

    def notify_participants(self, message_text):
        uids = self.get_participants()
        for uid in uids:
            self.send_notification(uid, message_text)

    def send_notification(self, uid, message_text):
        apn_key = db.reference("APNKeys/{}".format(uid)).get()
        message = messaging.Message(
                notification=messaging.Notification(
                    title=self.name,
                    body=message_text
                    ),
                token=apn_key
                )
        messaging.send(message)

def get_random_message_combination(message_list_1, message_list_2):
    index_1 = random.randint(0,len(message_list_1)-1)
    index_2 = random.randint(0,len(message_list_2)-1)
    reply = message_list_1[index_1] + " " + message_list_2[index_2]
    return reply

if __name__=="__main__":
    cred = credentials.Certificate("chatmessenger-6ea89-firebase-adminsdk-cq5gr-812b1f5441.json")
    firebase_admin.initialize_app(cred, {
        'databaseURL': 'https://chatmessenger-6ea89.firebaseio.com'
    })
    
    gf_bot = BetaBotDM()
    gf_bot.uid = "gf-bot"
    gf_bot.name = "Athena"
    replies = [
        "Hey, what did you get up to today?",
        "Hey! How was your day?",
        "Hi, I haven't heard from you today. How was your day?",
        "Hey! I missed you today. How have you been?"
            ]
    emojis = [u"\uE056", u"\uE414", u"\U0001F642", u"\U0001F607", u"\U0001F917"] 
    
    reply = get_random_message_combination(replies, emojis)
    #gf_bot.send_reply(reply)
