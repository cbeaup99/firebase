import firebase_admin
from firebase_admin import credentials
from firebase_admin import db
import time 
import bbc_scrape
import beta_bot

class Topics(object):
    def __init__(self):
        self.ref = db.reference("chatrooms/news")

    def set_news_topic(self, value_dict):
        initial = self.ref.get()
        initial.update(value_dict)
        self.ref.set(value=initial)

def get_news_room():
    bbc_data = get_bbc_data()
    timestamp = str(int(time.time()))
    news_room = {'timestamp': timestamp}
    news_room.update(bbc_data)
    return news_room

def get_bbc_data():
    bbc = bbc_scrape.BBCScraper()
    bbc.start_session()
    headline = bbc.get_promo_headline()
    description = bbc.get_promo_primer()
    bbc.driver.quit()
    return {"chatroom_name": headline, "description": description}


if __name__=="__main__":
    cred = credentials.Certificate("chatmessenger-6ea89-firebase-adminsdk-cq5gr-812b1f5441.json")
    firebase_admin.initialize_app(cred, {
        'databaseURL': 'https://chatmessenger-6ea89.firebaseio.com'
    })
    
    news_dict = get_news_room()
    topics = Topics()
    topics.set_news_topic(news_dict)
   
    bb = beta_bot.BetaBotConversation("news")
    bot_message = news_dict["chatroom_name"] + "\n\n" +  news_dict["description"]
    bb.send_bot_message(bot_message)
