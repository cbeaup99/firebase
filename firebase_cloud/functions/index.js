// // Create and Deploy Your First Cloud Functions
// // https://firebase.google.com/docs/functions/write-firebase-functions
//
'use strict';

const functions = require('firebase-functions');
const admin = require('firebase-admin');
admin.initializeApp();
admin.firestore().settings({ ignoreUndefinedProperties: true });

//exports.betaBot = functions.database.ref('/beta-bot-com/conversations/{conversationID}').onCreate((snapshot, context) => {
//    const original = snapshot.val();
//    console.log('BetaBot', context.params.conversationID, original);
//    const id = context.params.conversationID;
//    const betaBotMessage = 'What is weighing on your mind?';
//    snapshot.ref.child('latest_message/message').set(betaBotMessage);
//    return sendMessageToConversation(id, betaBotMessage); 
//});

exports.betaBotResponse = functions.database.ref('/beta-bot-com/conversations/{conversationID}').onUpdate( async (change, context) => {
    await new Promise(resolve => setTimeout(resolve, 1500));
    const prompt1 = "What is the worst that could happen?";
    const prompt2 = "Why is that so bad?";
    const prompt3 = "Often we feel better after talking about our feelings.";
    const promptArray = Array(prompt1, prompt2, prompt3);
    const id = context.params.conversationID;
    const message = change.after.val();
    const text = message.latest_message.message;
    if (text === "What is weighing on your mind?" || promptArray.indexOf(text) > -1 ) {
        return null;
    }
    await new Promise(resolve => setTimeout(resolve, 1500));
    var sendPrompt = promptArray[Math.floor(Math.random() * promptArray.length)];
    return sendMessageToConversation(id, sendPrompt);
});

function sendMessageToConversation(id, message_text) {
    const timestamp = getTimestamp();
    const messageID = `BetaBot_${timestamp}`; 
    const path = `/conversations/${id}/messages/${messageID}`;
    console.log('BetaBot', id, path);
    const date = getDateString();
    var db = admin.database();
    return db.ref(path).set({
        "content": message_text,
        "type": "text",
        "sender_name": "Beta Bot",
        "sender_uid": "beta-bot-com",
        "date": date,
        "timestamp": timestamp,
        "id": messageID,
        "isRead": false
    });
}


function getDateString() {
    var date = new Date().toISOString().replace(/\..+/, '');
    date = date + "Z";
    return date;
}

function getTimestamp() {
    const timestamp = Date.now();
    const tsString = timestamp.toString();
    const editedTs = tsString.slice(0, -3);
    return editedTs;
}
