#!/usr/local/env python2 

from selenium import webdriver

import time
from random import randint

from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys

from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC

from selenium.webdriver.chrome.options import Options

from webdriver_manager.chrome import ChromeDriverManager

import sys
sys.path.insert(0, "/Users/ccb/my_code/bfapi-github")
import bfapi

class BBCScraper(object):
    def __init__(self):
        self.heading_css = ".gs-c-promo-heading" 
        self.url = 'https://www.bbc.co.uk/news'
        self.driver = None

    def start_session(self):
        driver = webdriver.Chrome(ChromeDriverManager().install())
        driver.get(self.url)
        self.driver = driver
        logger = bfapi.create_logger()
        logger.info({'url': driver.current_url, 'time': time.asctime()})
        return driver

    def get_promo_headline(self):
        xpath = '//*[@id="nw-c-topstories-domestic"]/div/div/div[1]/div/div[1]/div/div/div[3]/div/a/h3'
        return self.get_text_for_xpath(xpath)

    def get_promo_primer(self):
        xpath = '//*[@id="nw-c-topstories-domestic"]/div/div/div[1]/div/div[1]/div/div/div[3]/div/p'
        return self.get_text_for_xpath(xpath)

    def get_text_for_xpath(self, xpath):
        element = self.driver.find_element_by_xpath(xpath)
        text = element.get_attribute("innerHTML")
        uni = text.encode('utf-8')
        return str(uni)


if __name__=="__main__":
    bbc = BBCScraper()
    bbc.start_session()
    headline = bbc.get_promo_headline()
    description = bbc.get_promo_primer()
    print(headline)
    print(description)
