import firebase_admin
from firebase_admin import credentials
from firebase_admin import db

class ConversationReader(object):
    def __init__(self):
        self.ref = db.reference("conversations")
        self.conversations = None

    def get_conversations(self):
        self.conversations = self.ref.get()

    def get_message_dict_list(self):
        convo_ids = self.conversations.keys()
        all_conversations = [self.conversations[convo_id] for convo_id in convo_ids]
        all_messages = [conversation["messages"] for conversation in all_conversations]
        all_messages = [message for message in all_messages if type(message) == dict]
        all_msgs = [message.values() for message in all_messages]
        flattened_all_msgs = [item for sublist in all_msgs for item in sublist]
        return flattened_all_msgs

    def get_all_senders(self, message_dict_list): 
        senders = [msg["sender_name"] for msg in message_dict_list]
        return senders

    def get_all_liked(self, message_dict_list):
        liked = [msg for msg in message_dict_list if "liked" in msg.keys()]
        liked = [msg for msg in liked if msg["liked"]]
        liked_senders = [msg["sender_uid"] for msg in liked]
        return liked_senders

    def count_unique_senders(self, senders):
        unique_senders = list(set(senders))
        sender_count = {}
        for name in unique_senders:
            sender_count[name] = senders.count(name)
        counts = {k.encode('ascii', 'ignore'): v for k, v in sender_count.items()}
        return counts 

class Analytics(object):
    def __init__(self):
        self.users_json = {}

    def get_users_json(self):
        self.users_json = db.reference("users").get()
        return self.users_json

    def get_user_dict(self):
        user_counts = {user["name"]: user["sent"] for user in self.users_json.values()}
        return user_counts

    def get_specific_users(self, user_dict):
        real_users = ['Charlotte Beauchamp','Richard Beauchamp','Suzanne Pike','Thomas ','Will Beauchamp','William Beauchamp', 'Vineet', 'Advik Chaudhary', 'Artur Begyan']   
        real_users_counts = {key: user_dict[key] for key in real_users}
        return real_users_counts

def print_analytics():
    cr = ConversationReader()
    cr.get_conversations()
    messages = cr.get_message_dict_list()
    senders = cr.get_all_senders(messages)
    counts = cr.count_unique_senders(senders)
    real_users = ['Thomas ', 'William Beauchamp', 'Richard Beauchamp', 'Will Beauchamp', 'Charlotte Beauchamp', 'Suzanne Pike', 'Olivia Pike']
    real_users_counts = {key: counts[key] for key in real_users}
    print(real_users_counts)
    return real_users_counts

def get_user_counts():
    users_json = db.reference("users").get()
    user_counts = {user["name"]: user["sent"] for user in users_json.values()}
    real_users = ['Charlotte Beauchamp','Richard Beauchamp','Suzanne Pike','Thomas ','Will Beauchamp','William Beauchamp', 'Vineet', 'Advik Chaudhary', 'Artur Begyan']   
    real_users_counts = {key: user_counts[key] for key in real_users}
    return real_users_counts

if __name__=="__main__":
    cred = credentials.Certificate("chatmessenger-6ea89-firebase-adminsdk-cq5gr-812b1f5441.json")
    firebase_admin.initialize_app(cred, {
        'databaseURL': 'https://chatmessenger-6ea89.firebaseio.com'
    })

    at = Analytics()
    json = at.get_users_json()
    user_count = at.get_user_dict()
    name_counts = at.get_specific_users(user_count)
    #name_counts = get_user_counts()
    total_count = sum(user_count.values())

    print(user_count)
    print(name_counts)
    column_names = ['','Charlotte Beauchamp','Richard Beauchamp','Suzanne Pike','Thomas ','Will Beauchamp','William Beauchamp', 'Vineet', 'Advik Chaudhary', 'Artur Begyan']
    from csv import DictWriter
    with open('metrics/history.csv', 'a') as f_object:
        dictwriter_object = DictWriter(f_object, fieldnames=column_names)
        dictwriter_object.writerow(name_counts)
        f_object.close()
    import pandas as pd
    df = pd.DataFrame.from_csv("metrics/history.csv")
    df["total"] = df.sum(axis=1)
    df["new_messages"] = df["total"] - df["total"].shift(1)
    print(df)
