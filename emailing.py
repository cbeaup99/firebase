import sys; sys.path.insert(0, "/Users/ccb/my_code/seamless/")
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
import smtplib
import sys
import traceback
#import parameters  # from bfapi - christie password
from ttools.debug import try_again_if_exception


class EmailMessage(object):
    def __init__(self, subject='', body='', html=None):
        self.subject = subject
        self.body = body
        self.html = html

    def __eq__(self, other):
        return (self.body == other.body and
                self.subject == other.subject and
                self.html == other.html)


class EmailAccount(object):
    email = None
    password = None


class GmailHTMLEmailer(object):
    server = 'smtp.gmail.com:587'

    def __init__(self, sender_account):
        self.sender_account = sender_account
        self.session = None

    def send_email(self, recipients, message):
        parsed_message = self._prepare_message(recipients, message)
        self._send(recipients, parsed_message)

    def _prepare_message(self, recipients, message):
        msg = MIMEMultipart('alternative')
        msg['Subject'] = message.subject
        msg['From'] = self.sender_account.email
        msg['To'] = ', '.join(recipients)
        if message.body:
            part1 = MIMEText(message.body, 'plain')
            msg.attach(part1)
        if message.html:
            part2 = MIMEText(message.html, 'html')
            msg.attach(part2)
        parsed_message = msg.as_string()
        return parsed_message

    def _send(self, recipients, message):
        self._prepare_session()
        self.session.sendmail(self.sender_account.email, recipients, message)
        self.session.quit()

    @try_again_if_exception(2, 0.5)
    def _prepare_session(self):
        self.session = smtplib.SMTP(self.server)
        # self.session.ehlo() # stackoverflow says you may need this
        self.session.starttls()
        self.session.login(self.sender_account.email, self.sender_account.password)


class SeamlessEmailAccount(EmailAccount):
    email = 'ccbeauchamp10@gmail.com'
    password = 'Aerodynamic224' 


class SeamlessEmailer(GmailHTMLEmailer):
    def __init__(self, sender_account=SeamlessEmailAccount()):
        self.sender_account = sender_account
        self.session = None


def email_exceptions(recipients):
    def wrapped_decorator(func):
        def wrapped_func(*args, **kwargs):
            try:
                return func(*args, **kwargs)
            except Exception:
                exc_type, exc_value, tb = sys.exc_info()
                message = traceback.format_exception(exc_type, exc_value, tb)
                _email_exception(message, recipients)
                raise exc_type
                raise exc_value
                raise tb
        return wrapped_func
    return wrapped_decorator


def _email_exception(message, recipients):
    emailer = SeamlessEmailer()
    subject = 'Exception: {}'.format(sys.argv)
    if isinstance(message, list):
        message = ''.join(message)
    email_message = EmailMessage(subject=subject, body=message)
    emailer.send_email(recipients, email_message)


def email_script_output(script_to_run, script_output, recipients, emailer=None):
    if not emailer:
        emailer = SeamlessEmailer()
    success_status = 'Success' if script_output.succeeded else 'FAILED'
    subject = script_to_run + ':{}'.format(success_status)
    body = 'Std Err:\n\t{}\nStd Out:\n\t{}'
    body = body.format(script_output.stderr, script_output.stdout)
    emailer.send_email(recipients=recipients, message=EmailMessage(subject=subject, body=body))


def parse_args():
    # We look for @ as a hack to find email address
    if '@' in sys.argv[-1]:
        command = sys.argv[-2]
        recipient = sys.argv[-1]
        email_stdout = False
    elif '@' in sys.argv[-2]:
        command = sys.argv[-3]
        recipient = sys.argv[-2]
        _email_stdout = sys.argv[-1]
        if _email_stdout in ['true', 'True', True]:
            email_stdout = True
    else:
        raise Exception('Unable to parse command line args')
    if '[' in recipient:
        recipients = eval(recipient)
    else:
        recipients = [recipient]
    return command, recipients, email_stdout


def execute_command(command):
    from fabric.api import local, warn_only
    error_msg = 'ipython gives false error status, no emails will be sent!'
    assert 'ipython' not in command, error_msg
    with warn_only():
        script_output = local(command, capture=True)
    return script_output


def main(emailer=SeamlessEmailer()):
    """
    Execute the code below to run any script and email the stdout:
        python ttools/emailing.py /full_path/script_to_email_stdout.sh recipient@gmail.com
    Script to execute should be a shell script, must be prefixed by its full path.
    """
    command, recipients, email_stdout = parse_args()
    script_output = execute_command(command)
    email_script_output(command, script_output, recipients, emailer)


if __name__ == '__main__':
    main()
