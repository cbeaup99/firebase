import firebase_admin
from firebase_admin import credentials
from firebase_admin import db
import time
import datetime
from beta_bot import BetaBotTopic
from random import randint

class QuoteBot(BetaBotTopic):
    def __init__(self, conversation_id):
        super(QuoteBot, self).__init__(conversation_id)
    
    def send_shakespeare_reply(self):
        self.send_file_reply("shakespeare.txt")

    def send_seneca_reply(self):
        self.send_file_reply("seneca.txt")

    def send_file_reply(self, filename):
        seneca_messages = self.get_text_file_messages(filename)
        seneca_msg = self.message_randomiser(seneca_messages)
        self.send_reply(seneca_msg)

    def message_randomiser(self, message_list):
        max_index = len(message_list) - 1
        index = randint(0, max_index)
        msg = message_list[index]
        return msg

    def get_text_file_messages(self, filename):
        message_list = [line.strip('\n') for line in open(filename, 'r')]
        return message_list



