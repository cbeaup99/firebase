import firebase_admin
from firebase_admin import credentials
from firebase_admin import db
import time
import datetime

#eg update_all(main_node="users", sub_node="likes", value="1")
def update_all(main_node="chatrooms", sub_node="last_updated", value="1"):
    ref = db.reference(main_node)
    chatrooms = ref.get()
    node_ids = chatrooms.keys()
    for node_id in node_ids:
        ref_string = main_node +"/"+ node_id +"/"+ sub_node
        room_ref = db.reference(ref_string)
        room_ref.set(value)

def update_streaks():
    ref = db.reference("users")
    user_json = ref.get()
    for uid in user_json.keys():
        seen_today = was_user_seen_today(user_json[uid]["last_seen"])
        set_streak_value(seen_today, uid) 

def was_user_seen_today(last_seen):
    if last_seen == "Online":
        return True
    last_seen_dt = datetime.datetime.strptime(last_seen, "%Y-%m-%dT%H:%M:%SZ")
    time_since_seen = datetime.datetime.now() - last_seen_dt
    return time_since_seen < datetime.timedelta(hours=24)

def set_streak_value(seen_today_bool, uid):
    path = "users/" + uid + "/streak"
    ref = db.reference(path)
    if seen_today_bool:
        ref.transaction(increment)
    else:
        ref.set(0)

def increment(current_value):
    return current_value + 1 if current_value else 1


if __name__=="__main__":
    cred = credentials.Certificate("chatmessenger-6ea89-firebase-adminsdk-cq5gr-812b1f5441.json")
    firebase_admin.initialize_app(cred, {
        'databaseURL': 'https://chatmessenger-6ea89.firebaseio.com'
    })
    update_streaks()
