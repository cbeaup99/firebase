import firebase_admin
from firebase_admin import credentials
from firebase_admin import db
import time
import datetime

class BetaBotConversation(object):
    def __init__(self, conversation_id):
        self.uid = "beta-bot-com"
        self.name = "Beta Bot"
        self.conversation_id = conversation_id
    
    def send_bot_message(self, message_text):
        message_id = "BetaBot" + str(int(time.time()))
        ref_string = self.get_reference_string(message_id)
        ref = db.reference(ref_string)
        message = self.get_message_dict(message_text, message_id)
        ref.set(message)

    def get_reference_string(self, message_id):
        location = "conversations/{}/messages/{}"
        location = location.format(self.conversation_id, message_id)
        return location

    def get_message_dict(self, message_text, message_id):
        timestamp = str(int(time.time()))
        date = datetime.datetime.now()
        formatted_date = date.strftime("%Y-%m-%dT%H:%M:%SZ")
        message = {"content": message_text,
                "date": formatted_date, 
                "id": message_id,
                "isRead": False,
                "liked": False,
                "sender_name": self.name,
                "sender_uid": self.uid,
                "timestamp": timestamp,
                "type": "text"
            }
        return message

class BetaBotTopic(BetaBotConversation):
    def __init__(self, conversation_id):
        super(BetaBotTopic, self).__init__(conversation_id)
        self.conversation_id = conversation_id

    def was_message_received(self):
        path = "conversations/" + self.conversation_id + "/messages"
        ref = db.reference(path)
        snapshot = ref.order_by_child("timestamp").get()
        key, value = snapshot.items()[-1]
        return (value["sender_uid"] != self.uid)

    def send_reply(self, message_text):
        self.send_bot_message(message_text)
        self.write_to_participant_nodes(message_text)

    def write_to_participant_nodes(self, message):
        participants = self.get_participants()
        for user in participants:
            path = user + "/conversations/" + self.conversation_id
            path += "/latest_message/message"
            ref = db.reference(path)
            ref.set(message)
        return

    def get_participants(self):
        chatroom_path = "chatrooms/{}".format(self.conversation_id)
        prt_path = chatroom_path + "/participant_uids"
        ref = db.reference(prt_path)
        participants = ref.get()
        prt_list = participants.split(", ")
        return prt_list


if __name__=="__main__":
    cred = credentials.Certificate("chatmessenger-6ea89-firebase-adminsdk-cq5gr-812b1f5441.json")
    firebase_admin.initialize_app(cred, {
        'databaseURL': 'https://chatmessenger-6ea89.firebaseio.com'
    })

    bb = BetaBotConversation("news")
    bb.send_bot_message("hello world!")
