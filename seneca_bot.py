from quote_bot import *


if __name__=="__main__":
    cred = credentials.Certificate("chatmessenger-6ea89-firebase-adminsdk-cq5gr-812b1f5441.json")
    firebase_admin.initialize_app(cred, {
        'databaseURL': 'https://chatmessenger-6ea89.firebaseio.com'
    })

    sn = QuoteBot("seneca")
    while True:
        if sn.was_message_received():
            time.sleep(2)
            sn.send_seneca_reply()
